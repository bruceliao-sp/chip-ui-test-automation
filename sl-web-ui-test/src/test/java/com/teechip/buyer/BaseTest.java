package com.teechip.buyer;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;
import util.UtilityManager;
import util.extentreport.ExtentManager;
import util.extentreport.ExtentTestManager;
import util.webdriver.WebDriverFactory;

import java.io.File;
import java.lang.reflect.Method;
import java.time.Duration;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;

import static util.webdriver.WebDriverFactory.getDriver;

public class BaseTest {
    //Extent report
    public static ThreadLocal<ExtentTest> testSection = new ThreadLocal<>();
    public static ThreadLocal<ExtentTest> testCase = new ThreadLocal<>();
    //Bean Factory
     protected ApplicationContext applicationContext =
            new ClassPathXmlApplicationContext("spring-sp-test-automation-bean.xml");

    public Properties testData;
    protected static ITestContext testContext;
    private static AtomicBoolean isBrowserInfoSet = new AtomicBoolean(false);

    @BeforeSuite
    public void beforeSuite() {}

    @BeforeTest
    public void beforeTest(ITestContext context) {
        try {
            String browser = context.getCurrentXmlTest().getLocalParameters().get("browser");
            String fileName = System.getProperty("user.dir") + "/target/extentreports/"
                    + context.getSuite().getName() + "/" + context.getCurrentXmlTest().getName()
                    + "-" + UtilityManager.getCurrentUnixTime() +".html";
            ExtentManager.createExtentReports(fileName, context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @BeforeClass
    public synchronized void beforeClass(ITestContext context) {
        try {
            String testName = context.getCurrentXmlTest().getName();
            ExtentReports extentReports = ExtentManager.getExtentReports(testName);
            String testTitle = getClass().getSimpleName();
            ExtentTest parent = ExtentTestManager.createTest(extentReports, testTitle);
            testSection.set(parent);
            testContext = context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @BeforeMethod
    public synchronized void beforeMethod (Method method, ITestContext context) {
        try {
            WebDriverFactory.setDriver(method, context);
            //prevent setting multiple browser info
            if (!isBrowserInfoSet.getAndSet(true)) {
                String testName = context.getCurrentXmlTest().getName();
                ExtentReports extentReports = ExtentManager.getExtentReports(testName);
                ExtentManager.setBrowserInfo(extentReports);
            }
            //Extent report
            //Create nodes for each test method in class
            ExtentTest child = testSection.get().createNode(method.getName());
            testCase.set(child);
            String testTitle = getClass().getSimpleName();
            testCase.get().assignCategory(testTitle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterMethod
    public synchronized void afterMethod(Method method, ITestContext context, ITestResult result) {
        try {
            getDriver().manage().deleteAllCookies();
            getDriver().quit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterTest
    public void afterTest(ITestContext context) {
        try {
            String testName = context.getCurrentXmlTest().getName();
            ExtentManager.getExtentReports(testName).flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterSuite
    public void afterSuite(ITestContext context) {}

    public boolean isElementPresent(By by) {
        try {
            getDriver().findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    //For those elements which disabled value keep in class attribute
    public boolean isElementEnabled(By by){
        try {
            boolean isEnable;
            isEnable = getDriver().findElement(by).getAttribute("class").contains("disabled");
            return isEnable;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean isAlertPresent() {
        try {
            getDriver().switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    public boolean isTextPresent(String text) {
        try {
            boolean isPresent = getDriver().getPageSource().contains(text);
            return isPresent;
        } catch (Exception e) {
            return false;
        }
    }

    public WebElement fluentWait(final By locator) {
        Wait<WebDriver> wait = new FluentWait<>(getDriver())
                .withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofSeconds(5))
                .ignoring(NoSuchElementException.class);
        WebElement webElement = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver webDriver) {
                return getDriver().findElement(locator);
            }
        });
        return webElement;
    }

    public void fluentWaitToBeClickable(WebElement webElement, int timeout, int polling) {
        Wait<WebDriver> wait = new FluentWait<>(WebDriverFactory.getDriver())
                .withTimeout(timeout, TimeUnit.SECONDS)
                .pollingEvery(polling, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.elementToBeClickable(webElement));
    }

    public void fluentWaitToBeDisplayed(WebElement webElement, int timeout, int polling) {
        Wait<WebDriver> wait = new FluentWait<>(WebDriverFactory.getDriver())
                .withTimeout(timeout, TimeUnit.SECONDS)
                .pollingEvery(polling, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.visibilityOf(webElement));
    }

    public Callable<Boolean> isFileExisted(File downloadFile) {
        return new Callable<Boolean>() {
            public Boolean call() {
                return downloadFile.exists() && downloadFile.length()>0;
            }
        };
    }

    public void scrollToBottomOfPage() {
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript(
                "window.scrollTo(0, document.body.scrollHeight)");
    }

    public void scrollToElementOfPage(WebElement element) {
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript(
                "arguments[0].scrollIntoView();", element);
    }

    public void scrollByCoordinatesOfPage(int x, int y) {
        ((JavascriptExecutor) WebDriverFactory.getDriver()).executeScript(
                "window.scrollBy(" + x + "," + y + ")");
    }
}