package com.teechip.buyer;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import util.UtilityManager;

import static util.webdriver.WebDriverFactory.getDriver;

public class CheckoutTest extends BaseTest {
    ProductDetailPagePO productDetailPagePO;
    CheckoutPagePO checkoutPagePO;
    CartPO cartPO;
    LandingPagePO landingPagePO;
    SearchResultPO searchResultPO;

    @BeforeClass
    public void setup() {
        testData = UtilityManager.setPropertiesFile(testContext.getCurrentXmlTest().getParameter("dataFile"));
    }

    @BeforeMethod
    public void methodSetup() {
        productDetailPagePO = (ProductDetailPagePO)applicationContext.getBean("ProductDetailPagePO");
        checkoutPagePO = (CheckoutPagePO)applicationContext.getBean("CheckoutPagePO");
        cartPO = (CartPO)applicationContext.getBean("CartPO");
        landingPagePO = (LandingPagePO)applicationContext.getBean("LandingPagePO");
        searchResultPO = (SearchResultPO)applicationContext.getBean("SearchResultPO");
        productDetailPagePO.setWebDriver(getDriver());
        checkoutPagePO.setWebDriver(getDriver());
        cartPO.setWebDriver(getDriver());
        landingPagePO.setWebDriver(getDriver());
        searchResultPO.setWebDriver(getDriver());
    }

    @Test
    public void checkoutWithCreditCard() {
        getDriver().get("https://teechip.ooapi.com/bruce01042021-1");
        testCase.get().log(Status.INFO, "Browser launched successfully");

        new WebDriverWait(getDriver(), 30).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
        fluentWaitToBeDisplayed(productDetailPagePO.buttonBuyItNow, 15, 2);
        productDetailPagePO.randomlySelectSizeAndColor();
        testCase.get().log(Status.INFO, "Randomly select size and color");
        productDetailPagePO.clickBuyItNowButton();
        testCase.get().log(Status.INFO, "Click buy it now");
        productDetailPagePO.clickProceedToCheckout();
        testCase.get().log(Status.INFO, "Click proceed to checkout");

//        new WebDriverWait(getDriver(), 30).until(
//                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
        checkoutPagePO.inputEmail(testData.getProperty("shippingAddressEmail"));
        checkoutPagePO.inputFullName(testData.getProperty("shippingAddressFullName"));
        checkoutPagePO.inputAddress(testData.getProperty("shippingAddressAddress"));
        checkoutPagePO.inputAptSuite(testData.getProperty("shippingAddressApt"));
        checkoutPagePO.selectState("California");
        checkoutPagePO.inputZip(testData.getProperty("shippingAddressZip"));
    }
}
