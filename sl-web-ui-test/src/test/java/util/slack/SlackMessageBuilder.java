package util.slack;

import org.json.simple.JSONObject;
import org.testng.ITestResult;
import util.Retry;

public class SlackMessageBuilder {
    String alertMsg;
    public String composeAlertMsg(ITestResult result) {
        JSONObject message = new JSONObject();
        message.put("text", "Test failed " + (Retry.maxTry + 1) + " times in a row. Please verify for potential issue."
                + "\nTest name: " + result.getMethod().getMethodName()
                + "\nEnvironment: " + result.getTestContext().getCurrentXmlTest().getLocalParameters().get("env")
                + "\nBrowser: " + result.getTestContext().getCurrentXmlTest().getLocalParameters().get("browser"));
        alertMsg = message.toJSONString();
        return alertMsg;
    }
}
