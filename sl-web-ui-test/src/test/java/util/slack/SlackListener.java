package util.slack;

import util.UtilityManager;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.io.IOException;
import java.util.Properties;

public class SlackListener extends TestListenerAdapter {

    //Slack
    public static Properties slackProp = UtilityManager.setPropertiesFile("slack.properties");
    private static final String WEBHOOK_URL = slackProp.getProperty("webhookUrl");
    public WebApiClient webAPIClient = new WebApiClient();
    public SlackMessageBuilder slackMessageBuilder = new SlackMessageBuilder();

    @Override
    public synchronized void onTestFailure(ITestResult result) {
        //Send Slack notification
        slackMessageBuilder.composeAlertMsg(result);
        try {
            String response = webAPIClient.post(WEBHOOK_URL, slackMessageBuilder.composeAlertMsg(result));
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Override
    public synchronized void onTestSuccess(ITestResult result) { }
}
