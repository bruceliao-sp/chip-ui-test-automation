package util;

import util.webdriver.WebDriverFactory;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.UnhandledAlertException;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class UtilityManager {

    public static Properties setPropertiesFile(String fileName) {
        Properties properties = new Properties();
        try {
            properties.load(UtilityManager.class.getClassLoader().getResourceAsStream(fileName));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return properties;
    }

    public static long getCurrentUnixTime() {
        Date date = new Date();
        return date.getTime() / 1000;
    }

    public static String getCurrentDataTimeString() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static void captureScreenshot(String fileName) {
        try {
            String path = "screenshots/" + fileName;

            File destination = new File(System.getProperty("user.dir") + "/target/screencapture/" + fileName +".png");
            //create directory if not exist
            if (!destination.getParentFile().exists()) {
                if (!destination.getParentFile().mkdirs()) {
                    throw new IOException("Created new directory failed.");
                }
            }

            try {
                FileUtils.copyFile(((TakesScreenshot) WebDriverFactory.getDriver()).getScreenshotAs(OutputType.FILE), destination);
            } catch (UnhandledAlertException e) {
                // Close alert to take screenshot again for debugging
                WebDriverFactory.getDriver().switchTo().alert().accept();
                FileUtils.copyFile(((TakesScreenshot) WebDriverFactory.getDriver()).getScreenshotAs(OutputType.FILE), destination);
            }
        } catch(IOException ioe) {
            throw new RuntimeException(ioe);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void createDirectory(String dirPath) {
        Path path = Paths.get(dirPath);
        if (!Files.exists(path)) {
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static double getImgDifferencePercent(BufferedImage img1, BufferedImage img2) {
        int width = img1.getWidth();
        int height = img1.getHeight();
        int width2 = img2.getWidth();
        int height2 = img2.getHeight();
        if (width != width2 || height != height2) {
            throw new IllegalArgumentException(String.format("Images must have the same dimensions: (%d,%d) vs. (%d,%d)"
                    , width, height, width2, height2));
        }

        long diff = 0;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                diff += pixelDiff(img1.getRGB(x, y), img2.getRGB(x, y));
            }
        }
        long maxDiff = 3L * 255 * width * height;

        return 100.0 * diff / maxDiff;
    }

    private static int pixelDiff(int rgb1, int rgb2) {
        int r1 = (rgb1 >> 16) & 0xff;
        int g1 = (rgb1 >> 8) & 0xff;
        int b1 = rgb1 & 0xff;
        int r2 = (rgb2 >> 16) & 0xff;
        int g2 = (rgb2 >> 8) & 0xff;
        int b2 = rgb2 & 0xff;
        return Math.abs(r1 - r2) + Math.abs(g1 - g2) + Math.abs(b1 - b2);
    }

    public static boolean isFilesIdentical(String file1Path, String file2Path) throws IOException {
        FileChannel ch1 = new RandomAccessFile(file1Path, "r").getChannel();
        FileChannel ch2 = new RandomAccessFile(file2Path, "r").getChannel();
        if (ch1.size() != ch2.size()) {
            System.out.println("Files have different length");
            return false;
        }
        long size = ch1.size();
        ByteBuffer m1 = ch1.map(FileChannel.MapMode.READ_ONLY, 0L, size);
        ByteBuffer m2 = ch2.map(FileChannel.MapMode.READ_ONLY, 0L, size);
        for (int pos = 0; pos < size; pos++) {
            if (m1.get(pos) != m2.get(pos)) {
                System.out.println("Files differ at position " + pos);
                return false;
            }
        }
        System.out.println("Files are identical.");
        return true;
    }

    public static String getSystem() {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("win")) {
            return "Windows";
        } else if (os.contains("nux") || os.contains("nix")) {
            return "Linux";
        } else if (os.contains("mac")) {
            return "Mac";
        } else if (os.contains("sunos")) {
            return "Solaris";
        } else {
            return "Other";
        }
    }
}
