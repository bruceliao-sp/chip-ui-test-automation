package util.webdriver;

import util.UtilityManager;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;

import java.lang.reflect.Method;
import java.util.Properties;

public class WebDriverFactory {

    public static ThreadLocal<WebDriver>  webDriverThreadLocal = new ThreadLocal<>();
    public static OptionManager optionManager = new OptionManager();
    public static Properties driverProp = UtilityManager.setPropertiesFile("driver.properties");


    public static synchronized WebDriver getDriver() {
        return webDriverThreadLocal.get();
    }

    public static synchronized void setDriver(Method method, ITestContext testContext) {
        String browser = testContext.getCurrentXmlTest().getParameter("browser");
//        String os = testContext.getCurrentXmlTest().getParameter("os");
//        String runOnGrid = testContext.getCurrentXmlTest().getParameter("runOnGrid");

        if (browser.equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver", driverProp.getProperty("chromeDriverMac"));
            webDriverThreadLocal.set(new ChromeDriver(optionManager.setChromeOptions().setPageLoadStrategy(PageLoadStrategy.NORMAL)));
        } else if (browser.equalsIgnoreCase("firefox")) {
            System.setProperty("webdriver.firefox.driver", driverProp.getProperty("geckoDriverMac"));
            webDriverThreadLocal.set(new FirefoxDriver(optionManager.setFirefoxOptions().setPageLoadStrategy(PageLoadStrategy.NORMAL)));
        }
    }

    public static synchronized Capabilities getCapabilities() {
        return ((RemoteWebDriver) webDriverThreadLocal.get()).getCapabilities();
    }

}
