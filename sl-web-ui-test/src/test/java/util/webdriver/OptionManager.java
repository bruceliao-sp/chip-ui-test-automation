package util.webdriver;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

import java.util.Hashtable;
import java.util.Map;

import static util.webdriver.WebDriverFactory.driverProp;

public class OptionManager {

    public ChromeOptions setChromeOptions() {

        ChromeOptions chromeOptions = new ChromeOptions();
        Map<String, Object> preferences = new Hashtable<>();
        preferences.put("profile.default_content_settings.popups", 0);
        preferences.put("download.prompt_for_download", "false");
        preferences.put("plugins.plugins_disabled", new String[]{
                "Adobe Flash Player", "Chrome PDF Viewer"});
        preferences.put("plugins.always_open_pdf_externally",true);

        chromeOptions.addArguments("--window-size=1440,900");
        chromeOptions.addArguments("start-maximized");
        if (Boolean.valueOf(driverProp.getProperty("is_headless", "true")))
            chromeOptions.addArguments("--headless");
        chromeOptions.addArguments("use-fake-ui-for-media-stream");
        chromeOptions.setExperimentalOption("prefs", preferences);

        return chromeOptions;
    }

    public FirefoxOptions setFirefoxOptions() {

        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setHeadless(Boolean.valueOf(driverProp.getProperty("is_headless", "false")));
        FirefoxProfile firefoxProfile = new FirefoxProfile();
        firefoxProfile.setPreference("browser.download.folderList",2);
        firefoxProfile.setPreference("browser.download.manager.showWhenStarting",false);
        firefoxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk"
                ,"application/csv,charset=utf-16,text/csv,application/vnd.ms-excel,application/pdf," +
                        "application/json,images/jpg,application/zip,application/octet-stream,text/plain");
        firefoxProfile.setPreference("browser.helperApps.alwaysAsk.force", false);
        firefoxProfile.setPreference("pdfjs.disabled", true);
        firefoxProfile.setPreference("plugin.disable_full_page_plugin_for_types", "application/pdf," +
                "application/vnd.adobe.xfdf,application/vnd.fdf,application/vnd.adobe.xdp+xml");
        firefoxProfile.setPreference("pdfjs.enabledCache.state", false);
        // Disable firefox update
        firefoxProfile.setPreference("app.update.enabled", false);
        firefoxProfile.setPreference("app.update.service.enabled", false);
        firefoxProfile.setPreference("app.update.auto", false);
        firefoxProfile.setPreference("app.update.staging.enabled", false);
        firefoxProfile.setPreference("app.update.silent", false);

        firefoxProfile.setPreference("media.gmp-provider.enabled", false);

        firefoxProfile.setPreference("extensions.update.autoUpdate", false);
        firefoxProfile.setPreference("extensions.update.autoUpdateEnabled", false);
        firefoxProfile.setPreference("extensions.update.enabled", false);
        firefoxProfile.setPreference("extensions.update.autoUpdateDefault", false);

        firefoxProfile.setPreference("lightweightThemes.update.enabled", false);
        firefoxProfile.setPreference("media.navigator.streams.fake", true);

        // SetProfile for FirefoxOptions
        firefoxOptions.setProfile(firefoxProfile);

        return firefoxOptions;
    }
}
