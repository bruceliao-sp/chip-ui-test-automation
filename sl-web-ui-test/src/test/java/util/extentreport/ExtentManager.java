package util.extentreport;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import util.webdriver.WebDriverFactory;
import org.testng.ITestContext;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ExtentManager {

    private static Map<String, ExtentReports> extentReportsMap = new HashMap<>();

    public static synchronized ExtentReports getExtentReports(String testName) {
        return extentReportsMap.get(testName);
    }

    public static synchronized ExtentReports createExtentReports(String fileName, ITestContext context) {
        ExtentSparkReporter reporter = new ExtentSparkReporter(fileName);
        ExtentReports extentReports = new ExtentReports();
        try {
            reporter.loadXMLConfig(new File(System.getProperty("user.dir") + "/extent-config.xml"));
        } catch(IOException e) {
            e.printStackTrace();
        }
        extentReports.attachReporter(reporter);
        setEnvInfo(extentReports, context);
        String testName = context.getCurrentXmlTest().getName();
        extentReportsMap.put(testName, extentReports);
        return extentReports;
    }

    private static synchronized void setEnvInfo(ExtentReports extentReports, ITestContext context) {
        extentReports.setSystemInfo("Name", context.getCurrentXmlTest().getName());
        extentReports.setSystemInfo("Env", context.getCurrentXmlTest().getLocalParameters().get("env"));
    }

    public static synchronized void setBrowserInfo(ExtentReports extentReports) {
        extentReports.setSystemInfo("Browser", WebDriverFactory.getCapabilities().getBrowserName());
        extentReports.setSystemInfo("Browser Version", WebDriverFactory.getCapabilities().getVersion());
    }
}
