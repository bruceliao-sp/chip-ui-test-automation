package util.testrail;

import util.UtilityManager;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.io.IOException;
import java.util.*;

public class TestrailListener extends TestListenerAdapter {
    //setup TestRail
    public static Properties testrailProp = UtilityManager.setPropertiesFile("testrail.properties");
    private Properties testsuiteProp;
    private static final String RAILS_ENGINE_URL = testrailProp.getProperty("rails_engine_url");
    private static final String TESTRAIL_USERNAME = testrailProp.getProperty("username");
    private static final String TESTRAIL_PASSWORD = testrailProp.getProperty("password");
    private static final int PASSED = 1;
    private static final int SKIPPED = 3;
    private static final int FAILED = 5;
    private static Map<String, String> testRunIdMap = new HashMap<>();
    static String testSuiteName;
    TestrailManager testrailManager = new TestrailManager(RAILS_ENGINE_URL,TESTRAIL_USERNAME, TESTRAIL_PASSWORD);

    @Override
    public synchronized void onStart(ITestContext context) {
        //Look up corresponding testsuite properties file
        testSuiteName = context.getSuite().getName();
        setTestsuiteProp(testSuiteName);
        //Create a RAT test run
        Map<String, Object> testRunMap = new HashMap<>();
        testRunMap.put("project_id", testrailProp.getProperty("sl_project_id"));
        testRunMap.put("smoke_type_id", testrailProp.getProperty("smoke_type_id"));
        testRunMap.put("regression_type_id", testrailProp.getProperty("regression_type_id"));
        testRunMap.put("suite_id", testsuiteProp.getProperty("suiteId"));
        testRunMap.put("test_context", context);

        try {
            //Add a new test run based on project id, suite id, and type id. If no type id is specified,
            // all test cases will be included in the test run by default.
            JSONObject testRunInfo = (JSONObject) JSONValue.parse(testrailManager.addTestRun(testRunMap));
            String testRunId = testRunInfo.get("id").toString();
            String testName = context.getCurrentXmlTest().getName();
            testRunIdMap.put(testName, testRunId);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public synchronized void onFinish(ITestContext context) {
        //Mark 'Completed' for the test run
//        try {
//            String testName = context.getCurrentXmlTest().getName();
//            testrailManager.closeRun(getTestRunId(testName));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public synchronized void onTestStart(ITestResult result) {
    }

    @Override
    public synchronized void onTestSuccess(ITestResult result) {
        String testName = result.getTestContext().getCurrentXmlTest().getName();
        String methodName = result.getMethod().getMethodName();
        String comment = methodName + " - Test Passed.";
        String caseId = testsuiteProp.getProperty(methodName + "_caseId");
        try {
            //add results to test run
            testrailManager.addResultForTestCase(getTestRunId(testName), caseId, PASSED, comment);
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public synchronized void onTestFailure(ITestResult result) {
        String testName = result.getTestContext().getCurrentXmlTest().getName();
        String methodName = result.getMethod().getMethodName();
        String comment = result.getThrowable().getMessage();
        String caseId = testsuiteProp.getProperty(methodName + "_caseId");
        try {
            //add result to test run
            testrailManager.addResultForTestCase(getTestRunId(testName), caseId, FAILED, comment);
        } catch(IOException ioe) {
            throw new RuntimeException(ioe);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public synchronized void onTestSkipped(ITestResult result) {
        String testName = result.getTestContext().getCurrentXmlTest().getName();
        String methodName = result.getMethod().getMethodName();
        String caseId = testsuiteProp.getProperty(methodName + "_caseId");
        try {
            //add results to test run
            testrailManager.addResultForTestCase(getTestRunId(testName), caseId, SKIPPED, "");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String getTestRunId(String testName) {
        return testRunIdMap.get(testName);
    }

    private void setTestsuiteProp(String name) {
        String propFileName = "testsuite." + name + ".properties";
        try {
            testsuiteProp = UtilityManager.setPropertiesFile(propFileName);
        } catch (Exception e) {
            throw new RuntimeException("Error on setting test suite properties file.");
        }
    }
}
