package com.teechip;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class PageObject {
    protected WebDriver webDriver;
    private static final int TIME_OUT = 20;

    public PageObject() {}

    public PageObject(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    public void setWebDriver(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    public void click(WebElement element) {
        element.click();
    }

    public void clickOnceVisible(WebElement element) {
        new WebDriverWait(webDriver, TIME_OUT).until(ExpectedConditions.visibilityOf(element));
        element.click();
    }

    public void clickOnceClickable(WebElement element) {
        new WebDriverWait(webDriver, TIME_OUT).until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }

    public void sendKeys(WebElement element, String text) {
        element.clear();
        element.sendKeys(text);
    }

    public void sendKeysOnceVisible(WebElement element, String text){
        new WebDriverWait(webDriver, TIME_OUT).until(ExpectedConditions.visibilityOf(element));
        element.clear();
        element.sendKeys(text);
    }

    public String getText(WebElement element){
        new WebDriverWait(webDriver, TIME_OUT).until(ExpectedConditions.visibilityOf(element));
        return element.getText();
    }

    public String getAttributeValue(By locator, String attributeName){
        new WebDriverWait(webDriver, TIME_OUT).until(ExpectedConditions.presenceOfElementLocated(locator));
        return  webDriver.findElement(locator).getAttribute(attributeName);
    }

    public void sendKeyboards(WebElement element, Keys keyboard){
        new WebDriverWait(webDriver, TIME_OUT).until(ExpectedConditions.visibilityOf(element));
        element.sendKeys(keyboard);
    }

    public void waitForElementPresent(By locator) {
        new WebDriverWait(webDriver, TIME_OUT).until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    protected void waitForElementNotDisplay(By locator) {
        // Wait few seconds to check element is not display as expected
        try{
            new WebDriverWait(webDriver, 3).until(ExpectedConditions.visibilityOfElementLocated(locator));
        }catch (TimeoutException e) {
        }
    }

    public void waitForElementEnabled(By locator) {
        try {
            // Wait for the element visible
            WebDriverWait wait = new WebDriverWait(webDriver, 3);
            wait.until((ExpectedCondition<Boolean>) driver -> webDriver.findElement(locator).isEnabled());
        } catch (Exception e) {
        }
    }

    // wait and get the refreshed element to prevent StaleElementReferenceException
    public WebElement waitForElementRefreshedAndVisible(WebElement element) {
        return new WebDriverWait(webDriver, 15).until(
                ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(element)));
    }

    public void waitForElementInvisible(By locator) {
        try {
            WebDriverWait wait = new WebDriverWait(webDriver, 10);
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
