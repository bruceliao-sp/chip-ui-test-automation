package com.teechip.buyer;

import com.teechip.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResultPO extends PageObject {

    @FindBy(xpath = "//div[@class='flex flex-wrap -m-p5 lg:-m-1']/div[@class='w-1/2 lg:w-1/3 p-p5 lg:p-1'][1]")
    WebElement firstItemInSearchResult;


    public void clickFirstItemInSearchResult() {
        click(firstItemInSearchResult);
    }

}
