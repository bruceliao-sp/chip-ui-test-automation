package com.teechip.buyer;

import com.teechip.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BuyerPageObject extends PageObject {

    //header --------------------------------
    @FindBy(xpath = "//*[@id=\"app\"]/div/div[3]/div/div/div[1]/div[2]/div/a/img")
    WebElement tcLogoHeader;

    @FindBy(xpath = "//a[@class='h-2 self-center pr-2 flex-none']//img[@alt='TeeChip']")
    public WebElement logoTeeChip;

    @FindBy(css = "input[type='text']")
    public WebElement searchBox;

    @FindBy(css = "i[class='mdi mdi-chevron-right fs-icon-1p5 float-right']")
    public WebElement searchSubmit;

    @FindBy(css = "a:nth-of-type(2) > div > i")
    public WebElement cartIcon;

    //footer ---------------------------------
    @FindBy(xpath = "//*[@id=\"app\"]/div/div[4]/div[2]/div[2]/div[1]/div/div[1]/img")
    WebElement tcLogoFooter;

    @FindBy(css = "div[class='w-1/2 px-p5 lg:d-n'] a[href='/orders/track'] span")
    public WebElement linkTrackOrder;

    @FindBy(css = "div[class~='overflow-auto'] i[class~='mdi-close']")
    public WebElement closeIconModal;

    //actions -----------------------------------------------------------------
    public void inputTextIntoSearchBox(String searchText) {
        this.click(searchBox);
        this.sendKeys(searchBox, searchText);
    }

    public void clickSearchSubmit() { click(searchSubmit);
    }

    public void clickTrackOrder() {
        click(linkTrackOrder);
    }

    public void clickCartIcon() {
        click(cartIcon);
    }

    public void clickCloseIconModal() {
        click(closeIconModal);
    }

    //getter ------------------------------------
    public WebElement getTcLogoHeader() {
        return tcLogoHeader;
    }

    public WebElement getTcLogoFooter() {
        return tcLogoFooter;
    }

    public WebElement getLogoTeeChip() {
        return logoTeeChip;
    }

    public WebElement getSearchBox() {
        return searchBox;
    }

    public WebElement getLinkTrackOrder() {
        return linkTrackOrder;
    }

    public WebElement getSearchSubmit() {
        return searchSubmit;
    }

    public WebElement getCartIcon() {
        return cartIcon;
    }

    public WebElement getCloseIconModal() {
        return closeIconModal;
    }
}
