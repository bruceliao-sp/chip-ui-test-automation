package com.teechip.buyer;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CartPO extends BuyerPageObject{

    @FindBy(xpath = "//span[text()='Proceed to Checkout']")
    public WebElement proceedToCheckoutButton;

    @FindBy(xpath = "//div[@class='floating ui mini circular sp-secondary-bg-color label']")
    public WebElement quantityIcon;

    @FindBy(xpath = "//div[@class='ui small active inline loader']")
    public WebElement subtotalPriceLoader;

    //@FindBy(xpath = "//span[text()='Discount']/../../following-sibling::div/span")
    @FindBy(xpath = "(//span[text()='Discount']/following::span[contains(text(), '-$')])[1]")
    public WebElement discountText;

    @FindBy(xpath = "//span[text()='Your Cart']/../following-sibling::div/span")
    public WebElement numberOfItemsInCart;

    @FindBy(xpath = "//span[text()='Your Cart']")
    public WebElement yourCartHeader;

    @FindBy(xpath = "//*[text()='Your cart is empty :(']")
    public WebElement emptyCartMessage;

    @FindBy(xpath = "//span[text()='Subtotal']/../../descendant::span[2])[1]")
    public WebElement cartSubtotal;

    @FindBy(xpath = "//*[@id=\"main-content\"]/div/div[1]/button")
    public WebElement continueShopping;

    // ====================================================================================================================
    //  Action
    // ====================================================================================================================
    public String getEmptyCartMessage() {
        return emptyCartMessage.getText();
    }

    public void clickContinueShopping() {
        click(continueShopping);
    }
}
