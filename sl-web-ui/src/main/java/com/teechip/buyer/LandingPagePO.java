package com.teechip.buyer;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class LandingPagePO extends BuyerPageObject{

    // ====================================================================================================================
    //  THRESHOLD COUPON BANNER Element
    // ====================================================================================================================
    @FindBy(xpath = "//div[@class='d-ib fs-md']/descendant::*[contains(text(), 'orders')]")
    public WebElement tc3ThresholdCouponBanner;

    // ====================================================================================================================
    //  TOP NAVIGATION Element
    // ====================================================================================================================
    public List<WebElement> bannerCarousel()
    {
        return webDriver.findElements(By.xpath("//div[contains(@class, 'br-full mx-p25 self-center pevt-auto')]"));
    }

    @FindBy(xpath = "//a[contains(@class, 'transition-carousel')]/following::"
            + "i[contains(@class, 'mdi-chevron-right')][1]")
    public WebElement bannerCarouselRightChevron;

    @FindBy(xpath = "//a[contains(@class, 'transition-carousel')]/following::"
            + "i[contains(@class, 'mdi-chevron-left')][1]")
    public WebElement bannerCarouselLeftChevron;

    @FindBy(xpath = "//i[@class='search icon']")
    public WebElement searchButton;

    public WebElement searchTextField()
    {
        return webDriver.findElement(By.xpath("//span[text()='What are you looking for?']/following::input[1]"));
    }

    @FindBy(xpath = "(//input[@class='search'])")
    public WebElement searchTextField;

    @FindBy(xpath = "//span[text()='Join to earn TeeChip Cash']")
    public WebElement linkJoinToEarnTeeChipCash;

    @FindBy(xpath = "(//span[text()='Log In']/following::span[text()=' / ']/following::span[text()='Sign Up'])[1]")
    public WebElement linkLogInSignUp;

    @FindBy(xpath = "//span[text()='TeeChip Cash']/following::i[contains(@class, 'chevron-down')][1]")
    public WebElement iconAccountDropdownChevron;

    @FindBy(xpath = "//span[text()='My Account']")
    public WebElement linkMyAccount;

    @FindBy(xpath = "//span[text()='Log Out']")
    public WebElement linkLogOut;

    @FindBy(xpath = "(//i[contains(@class, 'mdi-cart')])[1]")
    public WebElement iconCartTC3;

    @FindBy(xpath = "//i[@class='shop large icon']")
    public WebElement iconCartTC2;

    @FindBy(xpath = "//button[text()='Accept']")
    public WebElement buttonAcceptCookies;

    @FindBy(xpath = "//span[text()='Join to earn TeeChip Cash']")
    public WebElement spanJoinToEarnTCC;

    public WebElement TCCsections(String sSection)
    {
        return webDriver.findElement(By.xpath("//div[@class='fw-bold ml-1']//div/span[text()='"+ sSection +"']"));
    }

    @FindBy(xpath = "//div[contains(@class, 'w-full d-n lg:flex')]/..//../span[contains(text(),'TeeChip Cash')]")
    public WebElement TCCuserBanner;

    public WebElement tagSearchCD(String sTag)
    {
        return webDriver.findElement(By.xpath("//div[@class='flex -mx-1']/descendant::a[text()='"+sTag+"']"));
    }

    public WebElement childrenTagsCD(String sChildTag, String sTag)
    {
        return webDriver.findElement(By.xpath("//a[text()='"+sTag+"']/following::a[text()='"+sChildTag+"']"));
    }

    // ====================================================================================================================
    //  TOP NAVIGATION Action
    // ====================================================================================================================
    public int getBannerCarouselCount() {
        return bannerCarousel().size();
    }

}
