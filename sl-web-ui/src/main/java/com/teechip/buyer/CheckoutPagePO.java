package com.teechip.buyer;

import com.teechip.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CheckoutPagePO extends PageObject {

    @FindBy(xpath = "//*[text()='Checkout']")
    public WebElement headerCheckout;

    //--------------Shipping address------------------------------------------
    @FindBy(xpath = "//input[@type = 'email']")
    public WebElement textboxEmail;

    @FindBy(xpath = "//input[@name = 'name']")
    public WebElement textBoxFullName;

    @FindBy(xpath = "//input[@name = 'address-line1']")
    public WebElement textBoxAddress;

    @FindBy(xpath = "//input[@name = 'address-line2']")
    public WebElement textBoxAptSuite;

    @FindBy(xpath = "//input[@name = 'city']")
    public WebElement textboxCity; // careful, [1] is desktop [2] is mobile

    @FindBy(xpath = "//select[@name = 'state']")
    public WebElement selectDropDownState;

    @FindBy(xpath = "//input[@name = 'zip']")
    public WebElement textboxZip; // careful, [1] is desktop [2] is mobile

    @FindBy(xpath = "//span[text()='Change']")
    public WebElement linkChange;

    @FindBy(xpath = "//select[@name = 'country']")
    public WebElement selectDropDownCountry;

    //--------------------Payment info--------------------------------------
    @FindBy(xpath = "//input[@name = 'credit-card-number']")
    public WebElement textboxCardNumber;

    @FindBy(id = "expiration")
    public WebElement textboxExpirationDate;

    @FindBy(id = "cvv")
    public WebElement textboxCvv;
    //-----------------------------------------------------------------------

    @FindBy(xpath = "//button/span[text()='Place Your Order']")
    public WebElement buttonPlaceYourOrder;

    public void inputEmail(String email) {
        sendKeys(textboxEmail, email);
    }

    public void inputFullName(String fullName) {
        sendKeys(textBoxFullName, fullName);
    }

    public void inputAddress(String address) {
        sendKeys(textBoxAddress, address);
    }

    public void inputAptSuite(String aptSuite) {
        sendKeys(textBoxAptSuite, aptSuite);
    }

    public void inputCity(String city) {
        sendKeys(textboxCity, city);
    }

    public void selectState(String state) {
        By optionStateSelector = By.xpath("//select[@name='state']/option[text()='" + state + "']");
        WebElement optionState = webDriver.findElement(optionStateSelector);
        click(optionState);
    }

    public void inputZip(String zip) {
        sendKeys(textboxZip, zip);
    }

    public void selectCounty(String country) {
        By optionCountrySelector = By.xpath("//select[@name='state']/option[text()='" + country + "']");
        WebElement optionCountry = webDriver.findElement(optionCountrySelector);
        click(optionCountry);
    }

    public void clickLinkChange() {
        click(linkChange);
    }

    public void inputCreditCardNumber(String number) {
        sendKeysOnceVisible(textboxCardNumber, number);
    }

    public void inputExpirationDate(String exp) {
        sendKeys(textboxExpirationDate, exp);
    }

    public void inputCvv(String cvv) {
        sendKeys(textboxCvv, cvv);
    }

    public void clickPlaceYourOrder() {
        click(buttonPlaceYourOrder);
    }
}
