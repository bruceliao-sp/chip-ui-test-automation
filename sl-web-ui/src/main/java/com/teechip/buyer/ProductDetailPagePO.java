package com.teechip.buyer;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ProductDetailPagePO extends BuyerPageObject{

    @FindBy(css = ".mb-p25 .minw-2")
    public List<WebElement> allColors;

    @FindBy(css = ".d-ib > .br")
    public List<WebElement> allSizes;

    @FindBy(xpath = "//span[text()='Buy it now']")
    public WebElement buttonBuyItNow;

    @FindBy(xpath = "//span[text()='Proceed to checkout']")
    public WebElement buttonProceedToCheckout;



    public void randomlySelectSizeAndColor()
    {
//        new WebDriverWait(webdriver, 10).until(ExpectedConditions.visibilityOfAllElements(allSizes));
        int iColors = 0;
        int iSizes = 0;
        while (iColors <= 0 && iSizes <= 0) {
            iColors = (int) (Math.random() * (allColors.size() - 1));
            iSizes = (int) (Math.random() * (allSizes.size() - 1));
        }
        try {
            click(allColors.get(iColors));
            click(allSizes.get(iSizes));
        } catch (Exception e) {

        }
    }

    public void clickBuyItNowButton() {
        click(buttonBuyItNow);
    }

    public void clickProceedToCheckout() {
        click(buttonProceedToCheckout);
    }

}
